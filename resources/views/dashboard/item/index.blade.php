<!DOCTYPE html>
<html lang="ar">
@include('dashboard.layouts.header')


<body>
    @include('dashboard.layouts.nav-sidebar')

    <main id="main" class="main">

        <div class="pagetitle">
            <h1>Item List</h1>
        </div>


        <section class="section">
            <div class="row">
                <div class="col-lg-12">

                    <div class="card">
                        <div class="card-body">

                            <div class="col-12">
                                <div class="card recent-sales overflow-auto">

                                    <div class="card-body">
                                        <h5 class="card-title">Item <span>| List</span></h5>
                                        <table class="table table-borderless datatable">
                                            <thead>
                                                <tr>
                                                    <th scope="col">#</th>
                                                    <th scope="col">Name</th>
                                                    <th scope="col">Category</th>
                                                    <th scope="col">Price</th>
                                                    <th scope="col">Price after discount</th>
                                                    <th scope="col">Description</th>
                                                    <th scope="col">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($datas as $data)

                                                <tr>
                                                    <th scope="row"><a href="#">#{{ $data->id }}</a></th>
                                                    <td>{{ $data->name }}</td>
                                                    <td>
                                                        {{ $data->category->name }}
                                                        @if($data->category->depth >= 2)
                                                        ->
                                                        {{ $data->category->parent->name }}
                                                        @endif
                                                        @if($data->category->depth >= 3)
                                                        ->
                                                        {{ $data->category->parent->parent->name }}
                                                        @endif
                                                        @if($data->category->depth >= 4)
                                                        ->
                                                        {{ $data->category->parent->parent->parent->name }}
                                                        @endif
                                                    </td>
                                                    <td>{{ $data->price }}</td>
                                                    <td>{{ $data->price - $data->discount_price }}</td>
                                                    <td>{{ $data->description }}</td>
                                                    <td>
                                                        <a href="{{ route('items.edit', $data->id) }}"><span
                                                                class="badge bg-warning">Edit</span>
                                                        </a>
                                                        <a href="{{ route('items.destroy', $data->id) }}"><span
                                                                class="badge bg-danger">Delete</span>
                                                        </a>
                                                    </td>
                                                </tr>

                                                @endforeach

                                            </tbody>
                                        </table>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>

    </main>

    @include('dashboard.layouts.script')

</body>

</html>
