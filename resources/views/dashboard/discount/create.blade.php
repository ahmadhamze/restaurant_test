<!DOCTYPE html>
<html lang="ar">
@include('dashboard.layouts.header')


<body>
    @include('dashboard.layouts.nav-sidebar')

    <main id="main" class="main">

        <div class="pagetitle">
            <h1>Add new Discount</h1>
        </div>


        <section class="section">
            <div class="row">
                <div class="col-lg-12">

                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Add new discount</h5>

                            <!-- General Form Elements -->
                            <form action="{{ route('discount.store') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <h4>Select discount type :</h4>
                                    <div class="col-md-4">
                                        <div class="form-check">
                                            <input checked class="form-check-input" type="radio" name="type"
                                                id="gridRadios2" value="all">
                                            <label class="form-check-label" for="gridRadios2">
                                                All menu
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="type"
                                                id="gridRadios2" value="item">
                                            <label class="form-check-label" for="gridRadios2">
                                                Item discount
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="type"
                                                id="gridRadios2" value="category">
                                            <label class="form-check-label" for="gridRadios2">
                                                Category discount
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="name" class="col-form-label">Item </label>
                                        <select name="item_id" class="form-control">
                                            <option value="">None</option>
                                            @foreach ($items as $item)
                                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="name" class="col-form-label">Category</label>
                                        <select name="category_id" class="form-control">
                                            <option value="">None</option>
                                            @foreach ($categories as $category)
                                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="name" class="col-form-label">Amount * (%)</label>
                                        <input autocomplete="false" id="amount" type="number" min="1"
                                            max="100" name="amount" class="form-control" required>
                                    </div>
                                </div>
                                <br>
                                <div class="row mb-3">
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>

                            </form><!-- End General Form Elements -->

                        </div>
                    </div>

                </div>
            </div>
        </section>

    </main>

    @include('dashboard.layouts.script')

</body>

</html>
