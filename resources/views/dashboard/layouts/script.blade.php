<a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i
        class="bi bi-arrow-up-short mx-auto"></i></a>

</body>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous">
</script>
<!-- Vendor JS Files -->
<script src="{{ asset('restaurant_test/public/assets-dashboard/vendor/apexcharts/apexcharts.min.js') }}"></script>
<script src="{{ asset('restaurant_test/public/assets-dashboard/vendor/bootstrap/js/bootstrap.bundle.min.js') }}">
</script>
<script src="{{ asset('restaurant_test/public/assets-dashboard/vendor/chart.js/chart.umd.js') }}"></script>
<script src="{{ asset('restaurant_test/public/assets-dashboard/vendor/echarts/echarts.min.js') }}"></script>
<script src="{{ asset('restaurant_test/public/assets-dashboard/vendor/quill/quill.min.js') }}"></script>
<script src="{{ asset('restaurant_test/public/assets-dashboard/vendor/simple-datatables/simple-datatables.js') }}">
</script>
<script src="{{ asset('restaurant_test/public/assets-dashboard/vendor/tinymce/tinymce.min.js') }}"></script>
<script src="{{ asset('restaurant_test/public/assets-dashboard/vendor/php-email-form/validate.js') }}"></script>


<!-- Template Main JS File -->
<script src="{{ asset('restaurant_test/public/assets-dashboard/js/main.js') }}"></script>

