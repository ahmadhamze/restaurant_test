<!DOCTYPE html>
<html lang="ar">
@include('dashboard.layouts.header')


<body>
    @include('dashboard.layouts.nav-sidebar')

    <main id="main" class="main">

        <div class="pagetitle">
            <h1>Category List</h1>
        </div>


        <section class="section">
            <div class="row">
                <div class="col-lg-12">

                    <div class="card">
                        <div class="card-body">

                            <div class="col-12">
                                <div class="card recent-sales overflow-auto">

                                    <div class="card-body">
                                        <h5 class="card-title">Category <span>| List</span></h5>
                                        <table class="table table-borderless datatable">
                                            <thead>
                                                <tr>
                                                    <th scope="col">#</th>
                                                    <th scope="col">Name</th>
                                                    <th scope="col">Parent Category</th>
                                                    <th scope="col">Depth</th>
                                                    <th scope="col">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($datas as $data)

                                                <tr>
                                                    <th scope="row"><a href="#">#{{ $data->id }}</a></th>
                                                    <td>{{ $data->name }}</td>
                                                    <td>
                                                        @if($data->parent_id)
                                                        {{ $data->parent->name }}
                                                        @else
                                                        None
                                                        @endif
                                                    </td>
                                                    <td>{{ $data->depth }}</td>
                                                    <td>
                                                        <a href="{{ route('categories.edit', $data->id) }}"><span
                                                                class="badge bg-warning">Edit</span>
                                                        </a>
                                                        <a href="{{ route('categories.destroy', $data->id) }}"><span
                                                                class="badge bg-danger">Delete</span>
                                                        </a>
                                                    </td>
                                                </tr>

                                                @endforeach

                                            </tbody>
                                        </table>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>

    </main>

    @include('dashboard.layouts.script')

</body>

</html>
