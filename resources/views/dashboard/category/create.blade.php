<!DOCTYPE html>
<html lang="ar">
@include('dashboard.layouts.header')


<body>
    @include('dashboard.layouts.nav-sidebar')

    <main id="main" class="main">

        <div class="pagetitle">
            <h1>Add new category</h1>
        </div>


        <section class="section">
            <div class="row">
                <div class="col-lg-12">

                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Add new category</h5>

                            <!-- General Form Elements -->
                            <form action="{{ route('categories.store') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="name" class="col-form-label">Category Name *</label>
                                        <input autocomplete="false" id="name" type="text" name="name"
                                            class="form-control" required>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="name" class="col-form-label">Parent Category</label>
                                        <select name="parent_id" class="form-control">
                                            <option value="">None</option>
                                            @foreach ($categories as $category)
                                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <br>
                                <div class="row mb-3">
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>

                            </form><!-- End General Form Elements -->

                        </div>
                    </div>

                </div>
            </div>
        </section>

    </main>

    @include('dashboard.layouts.script')

</body>

</html>
