<!DOCTYPE html>
<html lang="ar">
@include('dashboard.layouts.header')


<body>
    @include('dashboard.layouts.nav-sidebar')

    <main id="main" class="main">

        <div class="pagetitle">
            <h1>Edit {{ $data->name }}</h1>
        </div>


        <section class="section">
            <div class="row">
                <div class="col-lg-12">

                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Edit {{ $data->name }}</h5>

                            <!-- General Form Elements -->
                            <form action="{{ route('categories.update',$data->id) }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="name" class="col-form-label">Category Name *</label>
                                        <input autocomplete="false" id="name" type="text" name="name"
                                            class="form-control" value="{{ $data->name }}" required>
                                    </div>
                                </div>
                                <br>
                                <div class="row mb-3">
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary">Update</button>
                                    </div>
                                </div>

                            </form><!-- End General Form Elements -->

                        </div>
                    </div>

                </div>
            </div>
        </section>

    </main>

    @include('dashboard.layouts.script')

</body>

</html>
