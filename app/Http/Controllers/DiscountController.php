<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Discount;
use App\Models\Item;
use Illuminate\Http\Request;

class DiscountController extends Controller
{
    public function index()
    {
        $discount = Discount::get();
        return view(
            'dashboard.discount.index',
            [
                'datas' => $discount,
            ]
        );
    }

    public function create()
    {
        return view(
            'dashboard.discount.create',
            [
                'categories' => Category::get(),
                'items' => Item::get(),
            ]
        );
    }
    public function store(Request $request)
    {
        $type=$request->type;
        if(!$request->category_id && !$request->item_id)
        {
            $type = 'all';
        }
        if (Discount::where('type', 'all')->count() > 0 && $type == 'all')
        return redirect()->back()->with('error', "There is a pre-discount on all menus, it is not possible to add a new all menu discount");


        $discount = new Discount();
        $discount->amount = $request->amount;
        $discount->type = $type;
        $discount->save();
        if ($request->category_id) {
            Category::where('id', $request->category_id)->update(['discount_id' => $discount->id]);
        }
        if ($request->item_id) {
            Item::where('id', $request->item_id)->update(['discount_id' => $discount->id]);
        }
        return to_route('discount.index');
    }

    public function destroy($discount_id)
    {


        Discount::where('id', $discount_id)->delete();
        Item::where('discount_id', $discount_id)->update(['discount_id' => null]);
        Category::where('discount_id', $discount_id)->update(['discount_id' => null]);
        return to_route('discount.index');
    }
}
