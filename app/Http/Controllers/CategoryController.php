<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Item;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::with('parent')->get();
        return view(
            'dashboard.category.index',
            [
                'datas' => $categories,
            ]
        );
    }

    public function create()
    {
        return view(
            'dashboard.category.create',
            [
                'categories' => Category::get(),
            ]
        );
    }
    public function store(Request $request)
    {
        if (!$request->parent_id)
            $depth = 1;
        else {
            $depth = Category::where('id', $request->parent_id)->first()->depth + 1;
        }

        if ($depth > 4)
            return redirect()->back()->with('error', 'Category depth above 4 ');

        $category = new Category();
        $category->name = $request->name;
        $category->parent_id = $request->parent_id;
        $category->depth = $depth;
        $category->save();
        return to_route('categories.index');
    }
    public function edit($category_id)
    {
        return view(
            'dashboard.category.edit',
            [
                'data' => Category::find($category_id),
                'categories' => Category::get(),
            ]
        );
    }
    public function update(Request $request, $category_id)
    {
        $category = Category::find($category_id);
        $category->name = $request->name;
        $category->save();
        return to_route('categories.index');
    }
    public function destroy($category_id)
    {
        if (Item::where('category_id', $category_id)->count() > 0 || Category::where('parent_id', $category_id)->count() > 0)
            return redirect()->back()->with('error', "Can't delete this category becais it's used");

        Category::where('id', $category_id)->delete();
        return to_route('categories.index');
    }
}
