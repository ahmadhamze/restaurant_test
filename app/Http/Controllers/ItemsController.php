<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Item;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class ItemsController extends Controller
{
    public function index()
    {
        $items = Item::with('category')->get();
        return view(
            'dashboard.item.index',
            [
                'datas' => $items,
            ]
        );
    }

    public function create()
    {
        return view(
            'dashboard.item.create',
            [
                'categories' => Category::get(),
            ]
        );
    }
    public function store(Request $request)
    {

        $item = new Item();
        $item->name = $request->name;
        $item->category_id = $request->category_id;
        $item->price = $request->price;
        $item->description = $request->desc;
        $item->save();
        return to_route('items.index');
    }
    public function edit($item_id)
    {
        return view(
            'dashboard.item.edit',
            [
                'data' => Item::find($item_id),
                'categories' => Category::get(),
            ]
        );
    }
    public function update(Request $request, $item_id)
    {
        $item = Item::find($item_id);
        $item->name = $request->name;
        $item->category_id = $request->category_id;
        $item->price = $request->price;
        $item->description = $request->desc;
        $item->save();
        return to_route('items.index');
    }
    public function destroy($item_id)
    {

        Item::where('id', $item_id)->delete();
        return to_route('items.index');
    }
}
