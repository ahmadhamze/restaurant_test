<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:6',
        ]);
        $error = array_values($validator->errors()->toArray());
        if ($validator->fails()) {
            return to_route('login')->with('error', $error);
        }

        if (auth()->attempt(['email' => $request->email, 'password' => $request->password])) {
            return redirect('/');
        } else {

            return to_route('login')->with('error', 'Email or password is wrong');
        }
    }

    public function logout()
    {
        Auth::logout();
        return to_route('login');
    }
}
