<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function parent()
    {
        return $this->belongsTo(Category::class, 'parent_id')->with('parent');
    }
    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id')->with('children');
    }
    public function discount()
    {
        return $this->belongsTo(Discount::class);
    }
}
