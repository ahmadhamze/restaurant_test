<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function category()
    {
        return $this->belongsTo(Category::class)->with('parent');
    }
    public function discount()
    {
        return $this->belongsTo(Discount::class);
    }
    public function getDiscountPriceAttribute()
    {
        $discount_value = 0;

        // all menu discount
        if (Discount::where('type', 'all')->count() > 0)
            $discount_value += Discount::where('type', 'all')->first()->amount;

        // item discount
        if ($this->discount_id)
            $discount_value = $this->discount->amount;

        // category discount
        if ($this->category->discount_id) {
            $discount_value += $this->category->discount->amount;
            if ($this->category->depth >= 2)
                $discount_value += $this->category->parent->discount->amount;
            if ($this->category->depth >= 3)
                $discount_value += $this->category->parent->parent->discount->amount;
            if ($this->category->depth >= 4)
                $discount_value += $this->category->parent->parent->parent->discount->amount;
        }

        return ($this->price * $discount_value) / 100;
    }
}
