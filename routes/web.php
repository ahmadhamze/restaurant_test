<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\DiscountController;
use App\Http\Controllers\ItemsController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/login', function () {
    return view('dashboard.login');
})->name('dashboard.sign-in');


Route::post('login', [AuthController::class, 'login'])->name('login');


Route::group([
    'middleware' => 'admin.auth'
], function () {
    Route::get('logout', [AuthController::class, 'logout'])->name('logout');
    Route::get('/', function () {
        return view('dashboard.index');
    });


    Route::group([
        'prefix' => 'categories'
    ], function () {
        Route::get('/', [CategoryController::class, 'index'])->name('categories.index');
        Route::get('/create', [CategoryController::class, 'create'])->name('categories.create');
        Route::post('/store', [CategoryController::class, 'store'])->name('categories.store');
        Route::get('/destroy/{category}', [CategoryController::class, 'destroy'])->name('categories.destroy');
        Route::get('/edit/{category}', [CategoryController::class, 'edit'])->name('categories.edit');
        Route::post('/update/{category}', [CategoryController::class, 'update'])->name('categories.update');
    });


    Route::group([
        'prefix' => 'items'
    ], function () {
        Route::get('/', [ItemsController::class, 'index'])->name('items.index');
        Route::get('/create', [ItemsController::class, 'create'])->name('items.create');
        Route::post('/store', [ItemsController::class, 'store'])->name('items.store');
        Route::get('/destroy/{category}', [ItemsController::class, 'destroy'])->name('items.destroy');
        Route::get('/edit/{category}', [ItemsController::class, 'edit'])->name('items.edit');
        Route::post('/update/{category}', [ItemsController::class, 'update'])->name('items.update');
    });

    Route::group([
        'prefix' => 'discount'
    ], function () {
        Route::get('/', [DiscountController::class, 'index'])->name('discount.index');
        Route::get('/create', [DiscountController::class, 'create'])->name('discount.create');
        Route::post('/store', [DiscountController::class, 'store'])->name('discount.store');
        Route::get('/destroy/{category}', [DiscountController::class, 'destroy'])->name('discount.destroy');
    });
});
